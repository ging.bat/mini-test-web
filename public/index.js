console.log('a')

function get_data() {
  const req = new XMLHttpRequest()
  req.open('get', '/mini-test-web/data.json')
  req.onload = function() {
    const res = req.response
    console.log(res)
    const data = JSON.parse(res)
    const htmlP1 = document.getElementById('p1')
    htmlP1.textContent = `A: ${data.a}, B: ${data.b}, Time: ${(new Date()).toLocaleString()}`;
  }
  req.send()
}
